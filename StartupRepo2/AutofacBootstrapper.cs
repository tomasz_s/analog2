﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using Autofac;
using Caliburn.Micro;
using ViewModels;
using Views;
using Models;

namespace Launcher
{
    public class AutofacBootstrapper : BootstrapperBase
    {
        private IContainer _container;

        public AutofacBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<WindowManager>().As<IWindowManager>().SingleInstance();
            builder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();

            builder.RegisterAssemblyModules(typeof(ViewModelModule).Assembly);
            builder.RegisterAssemblyModules(typeof(ViewModule).Assembly);
            builder.RegisterAssemblyModules(typeof(ModelModule).Assembly);

            _container = builder.Build();
        }

        protected override object GetInstance(Type service, string key)
        {
            return key == null
                ? _container.Resolve(service)
                : _container.ResolveNamed(key, service);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            var collectionType = typeof(IEnumerable<>).MakeGenericType(service);
            return (IEnumerable<object>) _container.Resolve(collectionType);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            return new[]
            {
                typeof(ViewModelModule).Assembly,
                typeof(ViewModule).Assembly,
            };
        }
    }
}