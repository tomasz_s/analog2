﻿using Models;
using Models.LogicRulesEngine;

namespace ViewModels.Rules
{
    public interface IRule
    {
        IEvaluable<LogEntry> GetFilter();
    }
}