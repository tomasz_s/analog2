﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Models;
using Models.LogicRulesEngine;

namespace ViewModels.Rules
{
    public class TextRule : PropertyChangedBase, IRule
    {
        public static readonly IEnumerable<TextRuleAction> PossibleActions = new List<TextRuleAction>
        {
            TextRuleAction.Contains,
            TextRuleAction.DoesNotContain
        };

        private TextRuleAction _selectedAction;
        private string _text;

        public TextRuleAction SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                if (value == _selectedAction) return;
                _selectedAction = value;
                NotifyOfPropertyChange(() => SelectedAction);
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                if (value == _text) return;
                _text = value;
                NotifyOfPropertyChange(() => Text);
            }
        }

        public IEvaluable<LogEntry> GetFilter()
        {
            switch (SelectedAction)
            {
                case TextRuleAction.Contains:
                    return GetContainsRule();
                case TextRuleAction.DoesNotContain:
                    return new NotRule<LogEntry>(GetContainsRule());
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private LogicRule<LogEntry> GetContainsRule()
        {
            return new LogicRule<LogEntry>(entry => entry.Message.Contains(Text));
        }
    }
}