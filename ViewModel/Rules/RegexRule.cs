﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Caliburn.Micro;
using Models;
using Models.LogicRulesEngine;

namespace ViewModels.Rules
{
    public class RegexRule : PropertyChangedBase, IRule
    {
        public static readonly IEnumerable<RegexRuleAction> PossibleActions = new List<RegexRuleAction>
        {
            RegexRuleAction.Matches,
            RegexRuleAction.DoesNotMatch
        };

        private string _pattern;
        private RegexRuleAction _selectedAction;

        public RegexRuleAction SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                if (value == _selectedAction) return;
                _selectedAction = value;
                NotifyOfPropertyChange(() => SelectedAction);
            }
        }

        public string Pattern
        {
            get { return _pattern; }
            set
            {
                if (value == _pattern) return;
                _pattern = value;
                NotifyOfPropertyChange(() => Pattern);
            }
        }

        public IEvaluable<LogEntry> GetFilter()
        {
            switch (SelectedAction)
            {
                case RegexRuleAction.Matches:
                    return GetMatchesRule();
                case RegexRuleAction.DoesNotMatch:
                    return new NotRule<LogEntry>(GetMatchesRule());
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private LogicRule<LogEntry> GetMatchesRule()
        {
            return new LogicRule<LogEntry>(entry => new Regex(Pattern).IsMatch(entry.Message));
        }
    }
}