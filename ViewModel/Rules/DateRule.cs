﻿using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Models;
using Models.LogicRulesEngine;

namespace ViewModels.Rules
{
    public class DateRule : PropertyChangedBase, IRule
    {
        public static readonly IEnumerable<DateRuleAction> PossibleActions = new List<DateRuleAction>
        {
            DateRuleAction.Before,
            DateRuleAction.After
        };

        private DateRuleAction _selectedAction;
        private DateTime _date;

        public DateRuleAction SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                if (value == _selectedAction) return;
                _selectedAction = value;
                NotifyOfPropertyChange(() => SelectedAction);
            }
        }

        public DateTime Date
        {
            get { return _date; }
            set
            {
                if (value.Equals(_date)) return;
                _date = value;
                NotifyOfPropertyChange(() => Date);
            }
        }

        public IEvaluable<LogEntry> GetFilter()
        {
            switch (SelectedAction)
            {
                case DateRuleAction.Before:
                    return GetBeforeRule();
                case DateRuleAction.After:
                    return GetAfterRule();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private LogicRule<LogEntry> GetBeforeRule()
        {
            return new LogicRule<LogEntry>(entry => entry.Date < Date);
        }

        private LogicRule<LogEntry> GetAfterRule()
        {
            return new LogicRule<LogEntry>(entry => entry.Date > Date);
        }
    }
}