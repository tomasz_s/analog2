using System.ComponentModel;

namespace ViewModels.Rules
{
    public enum TextRuleAction
    {
        [Description("Contains")] Contains,
        [Description("Does not contain")] DoesNotContain
    }
}