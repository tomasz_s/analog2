﻿namespace ViewModels.Rules
{
    public enum RegexRuleAction
    {
        Matches,
        DoesNotMatch
    }
}