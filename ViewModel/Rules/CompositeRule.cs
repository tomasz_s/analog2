﻿using System;
using System.Linq;
using Caliburn.Micro;
using Models;
using Models.LogicRulesEngine;

namespace ViewModels.Rules
{
    public class CompositeRule : PropertyChangedBase, IRule
    {
        private BindableCollection<IRule> _rules;
        private RuleGroupType _selectedType;

        public CompositeRule()
        {
            Rules = new BindableCollection<IRule>();
        }

        public RuleGroupType SelectedType
        {
            get { return _selectedType; }
            set
            {
                if (value == _selectedType) return;
                _selectedType = value;
                NotifyOfPropertyChange(() => SelectedType);
            }
        }

        public BindableCollection<IRule> Rules
        {
            get { return _rules; }
            set
            {
                if (Equals(value, _rules)) return;
                _rules = value;
                NotifyOfPropertyChange(() => Rules);
            }
        }

        public IEvaluable<LogEntry> GetFilter()
        {
            switch (SelectedType)
            {
                case RuleGroupType.And:
                    return new AndRule<LogEntry>(Rules.Select(rule => rule.GetFilter()));
                case RuleGroupType.Or:
                    return new OrRule<LogEntry>(Rules.Select(rule => rule.GetFilter()));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Remove(IRule rule)
        {
            Rules.Remove(rule);
        }

        public void AddTextRule()
        {
            Rules.Add(new TextRule());
        }

        public void AddDateRule()
        {
            Rules.Add(new DateRule());
        }

        public void AddRegexRule()
        {
            Rules.Add(new RegexRule());
        }

        public void AddCompositeRule()
        {
            Rules.Add(new CompositeRule());
        }
    }
}