﻿using Caliburn.Micro;
using Models;
using ViewModels.Rules;

namespace ViewModels
{
    public class FiltersWindowViewModel : Screen
    {
        private readonly ApplicationState _applicationState;
        private IRule _ruleViewModel;

        public FiltersWindowViewModel(ApplicationState applicationState)
        {
            _applicationState = applicationState;

            RuleViewModel = new CompositeRule();
        }

        public IRule RuleViewModel
        {
            get { return _ruleViewModel; }
            set
            {
                if (Equals(value, _ruleViewModel)) return;
                _ruleViewModel = value;
                NotifyOfPropertyChange(() => RuleViewModel);
            }
        }

        public void Filter()
        {
            var filter = RuleViewModel.GetFilter();
            _applicationState.SetFilter(filter);
        }
    }
}