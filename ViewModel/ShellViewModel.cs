﻿using System;
using Caliburn.Micro;
using Models;
using ViewModels.Services;

namespace ViewModels
{
    public class ShellViewModel : PropertyChangedBase
    {
        private readonly IFileService _fileService;
        private readonly IWindowManager _windowManager;
        private readonly ApplicationState _applicationState;
        private readonly FiltersWindowViewModel _filtersWindow;
        private string _text;

        public ShellViewModel(
            IFileService fileService,
            IWindowManager windowManager,
            FiltersWindowViewModel filtersWindow,
            ApplicationState applicationState)
        {
            _fileService = fileService;
            _windowManager = windowManager;
            _filtersWindow = filtersWindow;
            _applicationState = applicationState;

            _applicationState.ApplicationStateChanged += OnApplicationStateChanged;
        }

        public string Text
        {
            get { return _text; }
            set
            {
                if (value == _text) return;
                _text = value;
                NotifyOfPropertyChange(() => Text);
            }
        }

        public void OpenManyFiles()
        {
            var streams = _fileService.OpenMany();
            _applicationState.LoadFiles(streams);
        }

        public void OpenFilters()
        {
            _windowManager.ShowWindow(_filtersWindow);
        }

        private void OnApplicationStateChanged(object sender, ApplicationStateChangedEvntArgs args)
        {
            Text = args.FilteredLogEntries.ConvertToString();
        }
    }
}