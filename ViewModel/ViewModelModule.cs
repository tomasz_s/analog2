using Autofac;
using ViewModels.Rules;
using ViewModels.Services;

namespace ViewModels
{
    public class ViewModelModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ShellViewModel>()
                .SingleInstance();

            builder.RegisterType<FiltersWindowViewModel>()
                .SingleInstance();

            builder.RegisterType<FileService>()
                .As<IFileService>()
                .SingleInstance();
        }
    }
}