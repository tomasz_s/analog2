using System.Collections.Generic;
using System.IO;

namespace ViewModels.Services
{
    public interface IFileService
    {
        IEnumerable<Stream> OpenMany();
    }
}