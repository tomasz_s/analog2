﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Win32;

namespace ViewModels.Services
{
    public class FileService : IFileService
    {
        public IEnumerable<Stream> OpenMany()
        {
            var dialog = new OpenFileDialog
            {
                Multiselect = true
            };

            dialog.ShowDialog();
            var names = dialog.FileNames;
            return dialog.OpenFiles();
        }
    }
}