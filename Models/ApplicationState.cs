﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Models.Extensions;
using Models.LogicRulesEngine;

namespace Models
{
    public class ApplicationState
    {
        private IEvaluable<LogEntry> _filter;
        private Log _log;

        public event EventHandler<ApplicationStateChangedEvntArgs> ApplicationStateChanged;

        public void LoadFiles(IEnumerable<Stream> fileStreams)
        {
            _log = fileStreams
                .ReadAll()
                .ToLogParts()
                .ToLog();

            RaiseApplicationStateChanged(new ApplicationStateChangedEvntArgs(
                _filter != null
                    ? _log.Entries.Filter(_filter)
                    : _log.Entries));
        }

        public void LoadFiles(IEnumerable<string> s)
        {
            _log = s
                .ToLogParts()
                .ToLog();

            RaiseApplicationStateChanged(new ApplicationStateChangedEvntArgs(
                _filter != null
                    ? _log.Entries.Filter(_filter)
                    : _log.Entries));
        }

        public void SetFilter(IEvaluable<LogEntry> filter)
        {
            _filter = filter;

            RaiseApplicationStateChanged(new ApplicationStateChangedEvntArgs(
                _log != null
                    ? _log.Entries.Filter(_filter)
                    : Enumerable.Empty<LogEntry>()));
        }

        protected virtual void RaiseApplicationStateChanged(ApplicationStateChangedEvntArgs e)
        {
            ApplicationStateChanged?.Invoke(this, e);
        }
    }
}