﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Models.LogicRulesEngine;

namespace Models
{
    public static class LogService
    {
        public static IEnumerable<LogPart> ToLogParts(this IEnumerable<string> fileInputs)
        {
            return fileInputs
                .Select(input => new LogPart(input.ToEntries()));
        }

        public static Log ToLog(this IEnumerable<LogPart> logParts)
        {
            var parts = logParts.ToList();
            parts.Sort(new LogPartComparer());
            return new Log(parts.SelectMany(logPart => logPart.Entries));
        }

        private static IEnumerable<LogEntry> ToEntries(this string input)
        {
            var datePattern = @"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d.\d\d\d";
            var regexPattern = string.Format(@"(?<date>{0})( )(?<text>[\S\s]*?(?={0})|[\S\s]*)", datePattern);
            var regex = new Regex(regexPattern);
            return
                from Match match in regex.Matches(input)
                select new LogEntry(
                    Convert.ToDateTime(match.Groups["date"].ToString()),
                    match.Groups["text"].ToString());
        }

        public static IEnumerable<LogEntry> Filter(this IEnumerable<LogEntry> entries, IEvaluable<LogEntry> filter)
        {
            return entries.Where(filter.Evaluate);
        }

        public static string ConvertToString(this IEnumerable<LogEntry> entries)
        {
            var sb = new StringBuilder();
            foreach (var entry in entries)
            {
                sb.Append(entry);
            }
            return sb.ToString();
        }
    }
}