﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Models.Extensions
{
    public static class StreamExtension
    {
        public static string Read(this Stream stream)
        {
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static IEnumerable<string> ReadAll(this IEnumerable<Stream> streams)
        {
            return streams.Select(stream => stream.Read());
        }
    }
}