﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Models
{
    public class LogPartBuilder
    {
        private const string DatePattern = @"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d.\d\d\d";

        private static readonly Regex DateRegex = new Regex(
            $"^(?<date>{1}) (?<text>.*)",
            RegexOptions.Compiled);

        private readonly List<LogEntry> _completeEntries;
        private readonly StringBuilder _messageBuffer;
        private DateTime _dateBuffer;
        private bool _initialized;

        public LogPartBuilder()
        {
            _completeEntries = new List<LogEntry>();
            _messageBuffer = new StringBuilder();
            _initialized = false;
        }

        public void ProcessLine(string line)
        {
            var match = DateRegex.Match(line);

            if (_initialized)
            {
                if (match.Success)
                {
                    FlushBuffer();
                    BufferEntry(match);
                }
                else
                {
                    BufferMessage(line);
                }
            }
            else
            {
                if (!match.Success) return;
                _initialized = true;
                BufferEntry(match);
            }
        }

        public IEnumerable<LogEntry> Flush()
        {
            FlushBuffer();
            return _completeEntries;
        }

        private void BufferMessage(string line)
        {
            _messageBuffer.Append(line);
        }

        private void BufferEntry(Match match)
        {
            _dateBuffer = Convert.ToDateTime(match.Groups["date"].ToString());
            _messageBuffer.Append(match.Groups["text"]);
        }

        private void FlushBuffer()
        {
            var entry = new LogEntry(_dateBuffer, _messageBuffer.ToString());
            _completeEntries.Add(entry);
            _messageBuffer.Clear();
        }
    }
}