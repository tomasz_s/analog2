﻿using System;

namespace Models
{
    public class LogEntry
    {
        private readonly DateTime _date;
        private readonly string _message;

        public LogEntry(DateTime date, string message)
        {
            _date = date;
            _message = message;
        }

        public DateTime Date
        {
            get { return _date; }
        }

        public string Message
        {
            get { return _message; }
        }

        public override string ToString()
        {
            return $"{Date} {Message}";
        }
    }
}