﻿using System.Collections.Generic;

namespace Models
{
    public class LogPart
    {
        private readonly List<LogEntry> _entries;

        public LogPart(IEnumerable<LogEntry> entries)
        {
            _entries = new List<LogEntry>(entries);
        }

        public IEnumerable<LogEntry> Entries
        {
            get { return _entries; }
        }
    }
}