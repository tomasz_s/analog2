using System.Collections.Generic;
using System.Linq;

namespace Models
{
    public class LogPartComparer : IComparer<LogPart>
    {
        public int Compare(LogPart firstPart, LogPart secondPart)
        {
            if (firstPart == null || secondPart == null) return 0;
            if (!firstPart.Entries.Any() || !secondPart.Entries.Any()) return 0;
            if (firstPart.Entries.Last().Date <= secondPart.Entries.First().Date) return -1;
            if (firstPart.Entries.First().Date >= secondPart.Entries.Last().Date) return 1;
            return 0;
        }
    }
}