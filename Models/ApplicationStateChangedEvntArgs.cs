using System;
using System.Collections.Generic;

namespace Models
{
    public class ApplicationStateChangedEvntArgs : EventArgs
    {
        private readonly IEnumerable<LogEntry> _filteredLogEntries;

        public ApplicationStateChangedEvntArgs(IEnumerable<LogEntry> filteredLogEntries)
        {
            _filteredLogEntries = filteredLogEntries;
        }

        public IEnumerable<LogEntry> FilteredLogEntries
        {
            get { return _filteredLogEntries; }
        }
    }
}