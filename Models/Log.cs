﻿using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Log
    {
        public Log(IEnumerable<LogEntry> entries)
        {
            Entries = new List<LogEntry>(entries);
        }

        public List<LogEntry> Entries { get; }

        public override string ToString()
        {
            return Entries.ConvertToString();
        }
    }
}